#pragma once

#include "mumlib/Callback.hpp"

#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

#include <string>
#include <mumlib/enums.hpp>

namespace mumlib {

    constexpr int DEFAULT_OPUS_ENCODER_BITRATE = 16000;
    constexpr int DEFAULT_OPUS_DECODE_FORMAT = RawAudioFrameFormatType::INT16_T;

    using namespace std;
    using namespace boost::asio;

    class MumlibException : public runtime_error {
    public:
        MumlibException(string message) : runtime_error(message) { }
    };

    struct MumlibConfiguration {
        int opusEncoderBitrate = DEFAULT_OPUS_ENCODER_BITRATE;
        int RawAudioFrameFormatType = DEFAULT_OPUS_DECODE_FORMAT;
        // additional fields will be added in the future
    };

    struct _Mumlib_Private;


    class Mumlib : boost::noncopyable {
    public:
        Mumlib(Callback &callback);

        Mumlib(Callback &callback, io_service &ioService);

        Mumlib(Callback &callback, MumlibConfiguration &configuration);

        Mumlib(Callback &callback, io_service &ioService, MumlibConfiguration &configuration);

        virtual ~Mumlib();

        void connect(string host, int port, string user, string password);

        void disconnect();

        void run();

        ConnectionState getConnectionState();

        void sendAudioData(void *audioData, int sampleCount, RawAudioFrameFormatType srcType=RawAudioFrameFormatType::INT16_T);

        void sendAudioData(int16_t *pcmData, int pcmSize);

        void sendAudioData(float *floatData, int floatSize);

        void sendTextMessage(std::string message);

        void sendTextMessage(std::vector<uint32_t> session,
                             std::vector<uint32_t> channel_id,
                             std::vector<uint32_t> tree_id,
                             std::string message);

        void joinChannel(int channelId);

        void setSelfMute(bool muted);
        void setSelfDeafen(bool deafened);

    private:
        _Mumlib_Private *impl;
    };
}
